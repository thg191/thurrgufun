#!usr/env/bin python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt

# Lesum inn breytur
from breytur import *

# Reiknum fyrst S_s, þetta er fyrir öll hitastig og er tilgangslaust kannski
Ss = np.zeros(T.size)
for i in range(T.size):
    summa = 0
    for rot in rots:
        summa = summa + (rot*Ta[i])/(np.exp(rot*Ta[i]) - 1) - np.log(1 - np.exp(-rot*Ta[i]))
    Ss[i] = (R/2)*summa

# Reiknum Ss fyrir T = 320K
summa = 0
for rot in rots:
    summa = summa + (rot/320)/(np.exp(rot/320) - 1) - np.log(1 - np.exp(-rot/320))
S_s = (R/2)*summa

# Reiknum óvissu S_s, líka fyrir öll, líka tilgangslaust kannski
dSs = np.zeros(T.size)
for i in range(T.size):
    summa = 0
    for rot in rots:
        summa = summa + rot*((np.exp(-rot*Ta[i]) - 1 + rot*Ta[i]*np.exp(-rot*Ta[i]))/(np.power(np.exp(-rot*Ta[i]) - 1, 2)))
    dSs[i] = (R/2)*summa*dTa[i]

# Óvissa fyrir 320K:
for rot in rots:
    summa = summa + rot*((np.exp(-rot/320) - 1 + rot*np.exp(-rot*Ta[i]))/(320*(np.power(np.exp(-rot*Ta[i]) - 1, 2))))
dS_s = (R/2)*summa*(dT/(320**2))

# Reiknum nú S_g, tilgangslaust kannski
mug = dE - R*T*np.log(np.power(2*np.pi*k*T/h**2, 3/2)*(k*np.power(T, 2)*np.power(1-np.exp(-vib/T), -1))/(p*sig*rot0))
Sg = (dE - mug)/T + (7/2)*R + (R*vib/T)/(np.exp(vib/T) - 1)
# Óvissa S_g, tilgangslaust kannski
fasti = (3/2)*np.log(2*np.pi*m) - (5/2)*np.log(k) + np.log(p*sig*rot0*h**3)
breyti = -(7/2)*(1 + np.log(T)) + rot0*np.exp(-rot0*Ta)*T/(1 - np.exp(-rot0*Ta)) + np.log(1 - np.exp(-rot0*Ta))
dSg = R*(fasti + breyti)*dT 

# Reiknum nú S_g fyrir T = 320K, sirka p = 665 \pm 3:
mug1 = dE - R*320*np.log(np.power(2*np.pi*k*320/h**2, 3/2)*(k*np.power(320, 2)*np.power(1-np.exp(-vib/320), -1))/(665*sig*rot0))
S_g = (dE - mug1)/320 + (7/2)*R + (R*vib/320)/(np.exp(vib/320) - 1)
# Óvissa S_g í 320K:
fasti = -(3/2)*np.log(2*np.pi*m) - (5/2)*np.log(k) + np.log(665*sig*rot0) + 3*np.log(h)
breyti1 = -(7/2)*(1 + np.log(320)) - vib*np.exp(-vib/320)/(320**2*(1 - np.exp(-vib/320))) + np.log(1 - np.exp(-vib/320))
dS_g = abs(R*(fasti + breyti1)*dT) + 3/665

# Reiknum H út úr þessu
H_sub = 320*(S_g - S_s)
dH_sub = 320*(dS_g + dS_s)

# Setjum niðurstöður fram á grafi kannski, kemur í ljós ekki, prentum bara niðurstöður
print('S_s = ' + str(S_s) + " +- " + str(dS_s))
print('S_g = ' + str(S_g) + " +- " + str(dS_g))
print('H_s = ' + str(H_sub) + " +- " + str(dH_sub))
print(str(R))


best1, cov1 = np.polyfit(T, Ss, deg = 0, cov = True)
best2, cov2 = np.polyfit(T, Sg, deg = 0, cov = True)


fig1, ax1 = plt.subplots()
fig2, ax2 = plt.subplots()

ax1.errorbar(T, Ss, dSs, dT, fmt = '.')
ax2.errorbar(T, Sg, dSg, dT, fmt = '.')

x1,x2,y1,y2 = ax1.axis()
ax1.plot([x1,x2], [best1, best1])
ax1.set_xlim([x1,x2])

x1,x2,y1,y2 = ax2.axis()
ax2.plot([x1,x2], [best2, best2])
ax2.set_xlim([x1,x2])


