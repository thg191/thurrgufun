#! /usr/bin/env python
# -*- coding: utf8 -*-
import sys
import numpy as np
import matplotlib.pyplot as pl
from breytur import *
pl.rc('text.latex', preamble = r'\usepackage[T1]{fontenc}')
pl.rc('text.latex', preamble = r'\usepackage{siunitx}')

#from matplotlib import rcParams

const = np.log((2*np.pi*m*k*h**(-2))**(3/2)*k*(2*rot0)**(-1))

nasty = np.zeros(12)
for i in range(12):
    nasty[i] = 3.5*np.log(T[i]) + 0.5*np.sum(np.log(1-np.exp(-rots*Ta[i]))) - np.log(1 - np.exp(-vib*Ta[i]))

footloose = R*T*(const + nasty - lnp)
crystalloose = np.mean(footloose)
print(crystalloose)

dfootloose = np.zeros(12)
for i in range(12):
    dfootloose[i] = footloose[i]*T[i]*dTa[i] + R*(3.5*dT+ T[i]*dTa[i]*(np.sum(rots*np.exp(-rots*Ta[i])*(1 - np.exp(-rots*Ta[i]))**(-1)) + vib*np.exp(-vib*Ta[i])*(1-np.exp(-vib*Ta[i])))) + R*T[i]*dp[i]*p[i]**(-1)

fig1, ax1 = pl.subplots()

ax1.errorbar(T, footloose, dfootloose, dT, fmt='.', capsize = 3)
ax1.set(xlabel = r'\(T\)', ylabel = r'\(\Delta E_0^0\)')

x1,x2,y1,y2 = ax1.axis()
ax1.plot([x1,x2], [crystalloose,crystalloose])

print("Meðaltal er " + str(crystalloose) + " með óvissu " +\
        str(np.mean(dfootloose)) + ".")
fig1.show()
print("Viltu vista þetta sjitt? [Y,n]")
vista = input()
if "n" not in vista:
    fig1.savefig("threytt_graf.png")

