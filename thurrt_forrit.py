#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc
#plt.rc('text.latex', preamble = r'\usepackage[T1]{fontenc}')
#plt.rc('text.latex', preamble = r'\usepackage{siunitx}')

# Lesum inn gögn
from breytur import *

# Gerum gröf
fig1, ax1 = plt.subplots()
best, cov = np.polyfit(Ta, lnp, deg = 1, cov = True)
ax1.plot(np.array([-1, 10]), best[0]*np.array([-1, 10]) + best[1])
print('Besta lína fyrsta grafs: ' + str(best[0]) + 'x' + '+' + str(best[1]))
print('Með óvissu:')
print(np.sqrt(np.diagonal(cov)))
H_sub = -best[0]/R
print('Þá fæst H = ' + str(H_sub))
ax1.errorbar(Ta, lnp, dA/A + dT/T, dT*np.power(Ta,2), fmt = '.', capsize = 3)
ax1.set(xlabel = r'1/T \([\si{K}^{-1}]\)', ylabel = r'ln(p)')
ax1.set_xlim([min(Ta) - 0.00001 , max(Ta) + 0.00001])
ax1.set_ylim([4.0, 7.75])
fig1.show()
print('Vista mynd? [y/n]')
t = input()
if (t == 'y'):
    fig1.savefig('thurrt_graf_1.png')
plt.close(fig1)



# Seinni hluti
y = np.zeros(T.size)
tmp = np.zeros(T.size)
for i in range(T.size):
    tmp = 0.5*np.sum(np.log(1 - np.exp(-rots*Ta[i])))

y = lnp - 3.5*np.log(T) - tmp + np.log(1-np.exp(-vib*Ta))
    
dy = np.zeros(T.size)
for i in range(T.size):
    bigsum = 0
    for j in range(rots.size):
        bigsum = bigsum + rots[j]*np.exp(-rots[j]/T[i])/(1 - np.exp(-rots[j]/T[i]))
    dy[i] = dp[i]/p[i] + (7/2*T[i] - 0.5*(bigsum) + vib*np.exp(-vib/T[i])/(1 - np.exp(-vib/T[i])))*dTa[i]


fig2, ax2 = plt.subplots()
best2, cov2 = np.polyfit(Ta, y, deg = 1, cov = True)
ax2.errorbar(Ta, y, dy, dT*np.power(Ta,2), fmt = '.', capsize = 3)
a = min(Ta)-0.6*min(Ta)
b = max(Ta)+0.6*max(Ta)
ax2.plot([a,b], [best2[0]*a + best2[1], best2[0]*b + best2[1]])
bilx = max(Ta)-min(Ta)
bily = max(y)-min(y)
ax2.set_xlim([min(Ta) - 0.05*bilx , max(Ta) + 0.05*bilx])
ax2.set_ylim([min(y) - 0.25*bily , max(y) + 0.05*bily])
ax2.set(xlabel = r'1/T \([\si{K}^{-1}]\)', ylabel = u'Vinstri hlið jöfnu 16')
fig2.show()
print("Besta lína grafs 2: " + str(best2[0]) + "x + " + str(best2[1]))
print("Með óvissu: ")
print(np.sqrt(np.diagonal(cov2)))
print("Vista mynd? [y/n]")
t1 = input()
if t1 == 'y':
    fig2.savefig('thurrt_graf_2.png')




