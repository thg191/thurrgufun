import numpy as np
# Gögn lesin inn
dat = np.loadtxt('thurr_gogn.csv', delimiter = ',', skiprows = 1)

# Breytur
T       = dat[:,1]      #Hitastig
Ta      = dat[:,2]      #Andhverfa hitastigs
p       = dat[:,9]      #þrýstingur
lnp     = dat[:,11]     #ln(p)
A       = dat[:,7]      #Gleypni
R       = 8.3144598       #Gasfastinn
eps     = dat[:,8]      #Mólísogsstuðull
cte     = 0.1*R/eps     #Einhver fasti
c       = 299792458
k       = 1.3806503e-23 #Boltzmann fastinn
h       = 6.626068e-34
hck     = h*c/k         #Til einföldunar 
B0      = 0.037315*100  #Snúningsfasti
rot0    = hck*B0        #Snúningskennihiti
sig     = 2             #Samhverfutala I2
v0      = 213.3*100     #Titringstíðni
vib     = hck*v0        #Titringskennihiti
dE      = 5697.9137/R   #Hallatala fengin úr grafi 2
skp     = 23.7802       #Skurðp. við y-ás á grafi 2
m       = 2*126.9045*1.6605e-27   #Massi sameindarinnar

#Titringstíðnir sveifluhátta:
v       = np.array([21.0, 26.5, 33.0, 41.0, 49.0, 51.0, 58.0, 59.0, 75.4, 87.4, 180.7, 189.5])*100
rots    = h*c*v/k       #Titringskennihiti mism. sveifluhátta

# Óvissur
dT      = 0.1
dTa     = dT*np.power(Ta,2)
dA      = 0.02
dp      = p*(dA/A + dT/T)
ddE     = 101.8096/R
db      = 0.3140

